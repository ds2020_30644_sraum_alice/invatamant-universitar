use invatamant_universitar_1;

-- CADRU DIDACTIC -- 
CREATE LOGIN login_cadru_didactic WITH PASSWORD='cadru_didactic', DEFAULT_DATABASE=invatamant_universitar_1,
DEFAULT_LANGUAGE=[Romanian], CHECK_EXPIRATION=ON, CHECK_POLICY=ON;
CREATE USER user_cardu_didactic FOR LOGIN login_cadru_didactic WITH DEFAULT_SCHEMA=[dbo];
ALTER ROLE db_datareader ADD MEMBER user_cardu_didactic;
ALTER ROLE db_datawriter ADD MEMBER user_cardu_didactic;
ALTER ROLE db_accessadmin ADD MEMBER user_cardu_didactic;

-- ADMINISTRATOR -- 
CREATE LOGIN login_administrator WITH PASSWORD='admin', DEFAULT_DATABASE=invatamant_universitar_1,
DEFAULT_LANGUAGE=[Romanian], CHECK_EXPIRATION=ON, CHECK_POLICY=ON;
CREATE USER user_administrator FOR LOGIN login_administrator WITH DEFAULT_SCHEMA=[dbo];
ALTER ROLE db_owner ADD MEMBER user_administrator;

-- STUDENT -- 
CREATE LOGIN login_student WITH PASSWORD='student', DEFAULT_DATABASE=invatamant_universitar_1,
DEFAULT_LANGUAGE=[Romanian], CHECK_EXPIRATION=ON, CHECK_POLICY=ON;
CREATE USER user_student FOR LOGIN login_student WITH DEFAULT_SCHEMA=[dbo];
ALTER ROLE db_datareader ADD MEMBER user_student;
