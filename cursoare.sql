use invatamant_universitar_1;

-- cursoare --

-- 1. calcularea mediei aritmetice a fiecarui student
DECLARE cursor_note SCROLL CURSOR
	FOR 
		SELECT p.nume, p.prenume, count(n.nota), sum(nota)
		FROM persoane p join studenti s on p.id_pers = s.id_pers join note n on s.id_student = n.id_student
		GROUP BY p.nume, p.prenume;

exec calcul_medie;


CREATE PROCEDURE calcul_medie 
AS
BEGIN
	OPEN cursor_note;
	DECLARE @nume VARCHAR(50);
	DECLARE @prenume VARCHAR(50);
	DECLARE @nrNote INTEGER;
	DECLARE @suma FLOAT;

	FETCH FIRST FROM cursor_note INTO @nume, @prenume, @nrNote, @suma;
	WHILE @@FETCH_STATUS = 0
		BEGIN
			PRINT @nume + ' ' + @prenume + ' are media ' + CAST(@suma/@nrNote AS VARCHAR) FETCH NEXT FROM cursor_note INTO  @nume, @prenume, @nrNote, @suma;
		END;
	CLOSE cursor_note;
END

-- 2. calcularea mediei ponderate a fiecarui student
DECLARE cursor_note_cu_pondere SCROLL CURSOR
	FOR 
		SELECT p.nume, p.prenume, sum(m.nr_credite) , sum(nota * m.nr_credite)
		FROM persoane p join studenti s on p.id_pers = s.id_pers join note n on s.id_student = n.id_student join materii m on n.id_materie = m.id_materie
		GROUP BY p.nume, p.prenume;
exec calcul_medie_ponderata;

CREATE PROCEDURE calcul_medie_ponderata 
AS
BEGIN
	OPEN cursor_note_cu_pondere;
	DECLARE @nume VARCHAR(50);
	DECLARE @prenume VARCHAR(50);
	DECLARE @nrCredite INTEGER;
	DECLARE @sumaPonderata FLOAT;

	FETCH FIRST FROM cursor_note_cu_pondere INTO @nume, @prenume, @nrCredite, @sumaPonderata;
	WHILE @@FETCH_STATUS = 0
		BEGIN
			PRINT @nume + ' ' + @prenume + ' are media ponderata' + CAST(@sumaPonderata/@nrCredite AS VARCHAR) FETCH NEXT FROM cursor_note_cu_pondere INTO  @nume, @prenume, @nrCredite, @sumaPonderata;
		END;
	CLOSE cursor_note_cu_pondere;
END

-- query ca sa verific daca s-au facut bine calculele

SELECT p.nume, p.prenume, count(n.nota) as NR_NOTE, sum(nota * m.nr_credite) as SUMA_PONDERATA, n.nota, m.nr_credite, m.nume_materie
FROM persoane p join studenti s on p.id_pers = s.id_pers join note n on s.id_student = n.id_student join materii m on n.id_materie = m.id_materie
GROUP BY p.nume, p.prenume, nota, nr_credite, nume_materie;

-- pentru stergerea sau inchiderea cursorului 
close cursor_note_cu_pondere;
deallocate cursor_note_cu_pondere;
		