create database invatamant_universitar_1
go
use invatamant_universitar_1
go
create table persoane (id_pers int IDENTITY(1,1) primary key ,
                        cnp varchar(13) not null unique,
						nume varchar(50) not null,
						prenume varchar(50) not null,
						varsta int check (varsta >= 18),
						adresa varchar(150))
go
create table universitati ( id_univ int IDENTITY(1,1) primary key,
							nume_univ varchar(100) not null unique,
							id_rector int not null foreign key references persoane(id_pers))
go
create table facultati ( id_facult int IDENTITY(1,1) primary key,
						 id_univ int not null foreign key references universitati(id_univ),
				         nume_facult varchar(100) not null unique,
						 id_decan int not null foreign key references persoane(id_pers))
go
create table departamente ( id_depart int IDENTITY(1,1) primary key,
							id_facult int not null foreign key references facultati(id_facult),
							nume_depart varchar(100) not null unique,
							id_sef_depart int not null foreign key references persoane(id_pers))
go
create table cadre_did ( id_cadru_didact int IDENTITY(1,1) primary key,
						 id_depart int not null foreign key references departamente(id_depart),
						 id_pers int not null foreign key references persoane(id_pers))
go
create table an_studiu ( id_an_univ int IDENTITY(1,1) primary key,
						 id_sef_an int not null foreign key references persoane(id_pers))
go
create table serii ( id_serie int IDENTITY(1,1) primary key,
					 nume_serie varchar(50) not null,
					 id_sef_serie int not null foreign key references persoane(id_pers),
					 id_an int not null foreign key references an_studiu(id_an_univ))
go
create table grupe ( id_grupa int IDENTITY(1,1) primary key,
					 id_serie int not null foreign key references serii(id_serie),
					 id_sef_grupa int not null foreign key references persoane(id_pers))
go
create table studenti ( id_student int IDENTITY(1,1) primary key,
                        nr_matricol varchar(8) not null unique,
						id_pers int not null foreign key references persoane(id_pers),
						id_grupa int not null foreign key references grupe(id_grupa))
go
create table materii ( id_materie int IDENTITY(1,1) primary key,
					   nume_materie varchar(50) not null unique,
					   nr_credite int DEFAULT 2 check (nr_credite BETWEEN 2 AND 8))
go
create table note ( id_note int IDENTITY(1,1) primary key,
					id_student int not null foreign key references studenti(id_student),
					id_cadru_didact int not null foreign key references cadre_did(id_cadru_didact),
                    data_examen date DEFAULT GETDATE(),
					tip_sesiune varchar(20) not null, 
					tip_examinare varchar(20),  
					id_materie int not null foreign key references materii(id_materie),
					nota float DEFAULT 1.00 check (nota >= 1.00),
					situatie varchar(20) DEFAULT 'absent',
					CONSTRAINT tipuri_sesiune_ck CHECK (tip_sesiune IN ('iarna', 'vara', 'toamna')),
					CONSTRAINT tipuri_examinare_ck CHECK (tip_examinare IN ('laborator', 'examen', 'marire', 'restanta')),
					CONSTRAINT situatie_ck CHECK (situatie IN('absent', 'admis', 'respins')))

go
create table materii_cadru_did( id_mcd int IDENTITY(1,1) primary key,
								id_materie int not null foreign key references materii(id_materie),
								id_cadru_didact int not null foreign key references cadre_did(id_cadru_didact))