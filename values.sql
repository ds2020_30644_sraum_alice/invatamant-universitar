use invatamant_universitar_1;
-- PERSON --
INSERT INTO persoane (cnp, nume, prenume, varsta) VALUES ('2880418018010', 'Sraum', 'Alice', 21);
INSERT INTO persoane (cnp, nume, prenume, varsta) VALUES ('2950806444801', 'Pogacean', 'Rahela', 23);
INSERT INTO persoane (cnp, nume, prenume, varsta) VALUES ('2970630441215', 'Nitu', 'Andreea', 22);
INSERT INTO persoane (cnp, nume, prenume, varsta) VALUES ('2971113284480', 'Covaci', 'Alexandra', 22);
INSERT INTO persoane (cnp, nume, prenume, varsta) VALUES ('1980601244795', 'Marica', 'Gabriel', 22);
INSERT INTO persoane (cnp, nume, prenume, varsta) VALUES ('1020427394998', 'Catelina', 'Ioan', 40);
INSERT INTO persoane (cnp, nume, prenume, varsta) VALUES ('1970107171357', 'Luncan', 'Razvan', 42);
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1870513107093', 'Strutui', 'Benjamin', 41, 'Carei');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('2940818100769', 'Iloie', 'Madalina', 39, 'Bistrita');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('2010118240218', 'Neamtu', 'Malina', 39, 'Bacau');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1901221368371', 'Pop', 'Alin', 38, 'Cluj');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1891226090931', 'Pop', 'Mihai', 33, 'Cluj');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('2860206411793', 'Nistor', 'Mara', 35, 'Baia Mare');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1940328214069', 'Santa', 'Iulia', 30, 'Baia Mare');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('2891207304320', 'Cucui', 'Mihai', 30, 'Blaj');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('2891207304321', 'Isaila', 'Octavian', 31, 'Blaj');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('2891207304322', 'Martin', 'Dariu', 32, 'Cugir');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('2891207304323', 'Scarlat', 'Alin', 33, 'Sebes');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('2891207304324', 'Todescu', 'Andrei', 34, 'Sebes');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('2891207304325', 'Popa', 'Rares', 35, 'Sebes');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('2891207304326', 'Romanoiu', 'Adrian', 36, 'Valcea');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('2891207304327', 'Rentea', 'Robert', 37, 'Valcea');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('2891207304328', 'Vraja', 'Iulian', 38, 'Valcea');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('2891207304329', 'Fat', 'Luiza', 25, 'Baia Mare');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('2891207304339', 'Salajanu', 'Ana Maria', 24, 'Baia Mare');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1891207304329', 'Paun', 'Bogdan', 24, 'Baia Mare');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1892207304329', 'Maris', 'Andreea', 25, 'Baia Mare');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1891307304329', 'Soran', 'Andreea', 24, 'Zalau');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1891217304329', 'Szasz', 'Henrietta', 24, 'Baia Mare');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('2891217304329', 'Pop', 'Sorin', 24, 'Baia Mare');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1991217304329', 'Racz', 'Mirel', 24, 'Baia Mare');

INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1890722103252', 'Pop', 'Vasile', 24, 'Cluj');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('5021020065326', 'Vasiu', 'Miruna', 24, 'Cluj');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1920207115961', 'Nistor', 'Brana', 24, 'Cluj');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1920207115962', 'Georgiu', 'Giorgiana', 24, 'Cluj');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1920207115963', 'Termure', 'Adriana', 24, 'Cluj');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1920207115964', 'Termure', 'Gabriel', 24, 'Cluj');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1920207115965', 'Postolachi', 'Alexei', 24, 'Cluj');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1920207115966', 'Potor', 'Dan', 24, 'Cluj');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1920207115967', 'Popovici', 'Andrei', 24, 'Cluj');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1920207115968', 'Balus', 'Cosmina', 24, 'Cluj');

INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1920217115968', 'Balus', 'Mihai', 25, 'Cluj');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1920227115968', 'Balus', 'George', 25, 'Cluj');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1920237115968', 'Balus', 'Ionut', 25, 'Cluj');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1920247115968', 'Balus', 'Iulia', 25, 'Cluj');
INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1920257115968', 'Balus', 'Crina', 25, 'Cluj');

select * from persoane;

-- UNIVERSITATI --
INSERT INTO universitati (nume_univ, id_rector) VALUES ('UNIVERSITATEA TEHNICA', 6);

select * from universitati;

-- FACULTATI --
INSERT INTO facultati (id_univ, nume_facult, id_decan) VALUES (1, 'Facultatea de Automatic? ?i Calculatoare', 7);
INSERT INTO facultati (id_univ, nume_facult, id_decan) VALUES (1, 'Facultatea de Arhitectur? ?i Urbanism', 8);
INSERT INTO facultati (id_univ, nume_facult, id_decan) VALUES (1, 'Facultatea de Construc?ii', 9);
INSERT INTO facultati (id_univ, nume_facult, id_decan) VALUES (1, 'Facultatea de Ingineria Materialelor ?i a Mediului', 10);

select * from facultati;

-- DEPARTAMENTE --
INSERT INTO departamente (id_facult, nume_depart, id_sef_depart) VALUES (1, 'Calculatoare', 3);
INSERT INTO departamente (id_facult, nume_depart, id_sef_depart) VALUES (1, 'Automatic?', 2);
INSERT INTO departamente (id_facult, nume_depart, id_sef_depart) VALUES (1, 'Matematica', 1);

select * from departamente;

-- CADRE DIDACTICE --
INSERT INTO cadre_did (id_depart, id_pers) VALUES (1, 4);
INSERT INTO cadre_did (id_depart, id_pers) VALUES (1, 11);
INSERT INTO cadre_did (id_depart, id_pers) VALUES (2, 5);
INSERT INTO cadre_did (id_depart, id_pers) VALUES (2, 12);
INSERT INTO cadre_did (id_depart, id_pers) VALUES (3, 13);
INSERT INTO cadre_did (id_depart, id_pers) VALUES (3, 14);

select * from cadre_did;

-- AN STUDIU -- 
INSERT INTO an_studiu (id_sef_an) VALUES (15);
INSERT INTO an_studiu (id_sef_an) VALUES (16);
INSERT INTO an_studiu (id_sef_an) VALUES (17);
INSERT INTO an_studiu (id_sef_an) VALUES (18);

select * from an_studiu;

-- SERII --
INSERT INTO serii (nume_serie, id_sef_serie, id_an) VALUES ('A', 19, 1);
INSERT INTO serii (nume_serie, id_sef_serie, id_an) VALUES ('B', 20, 1);

INSERT INTO serii (nume_serie, id_sef_serie, id_an) VALUES ('A', 21, 2);
INSERT INTO serii (nume_serie, id_sef_serie, id_an) VALUES ('B', 22, 2);

INSERT INTO serii (nume_serie, id_sef_serie, id_an) VALUES ('A', 23, 3);
INSERT INTO serii (nume_serie, id_sef_serie, id_an) VALUES ('B', 28, 3);

INSERT INTO serii (nume_serie, id_sef_serie, id_an) VALUES ('A', 25, 4);
INSERT INTO serii (nume_serie, id_sef_serie, id_an) VALUES ('B', 26, 4);

select * from serii;

-- GRUPE -- nu am facut grupele numai pentru seria A anul 1
INSERT INTO grupe (id_serie, id_sef_grupa) VALUES (1, 23);
INSERT INTO grupe (id_serie, id_sef_grupa) VALUES (1, 30);
INSERT INTO grupe (id_serie, id_sef_grupa) VALUES (1, 31);
INSERT INTO grupe (id_serie, id_sef_grupa) VALUES (1, 34);
INSERT INTO grupe (id_serie, id_sef_grupa) VALUES (1, 35);

select * from grupe;

-- STUDENTI --
INSERT INTO studenti (nr_matricol, id_pers, id_grupa) VALUES ('12345678', 36, 1);
INSERT INTO studenti (nr_matricol, id_pers, id_grupa) VALUES ('13245673', 37, 1);

INSERT INTO studenti (nr_matricol, id_pers, id_grupa) VALUES ('13345622', 38, 2);
INSERT INTO studenti (nr_matricol, id_pers, id_grupa) VALUES ('15678332', 39, 2);

INSERT INTO studenti (nr_matricol, id_pers, id_grupa) VALUES ('15532177', 40, 3);
INSERT INTO studenti (nr_matricol, id_pers, id_grupa) VALUES ('12334211', 41, 3);

INSERT INTO studenti (nr_matricol, id_pers, id_grupa) VALUES ('14452273', 42, 4);
INSERT INTO studenti (nr_matricol, id_pers, id_grupa) VALUES ('12455322', 43, 4);

INSERT INTO studenti (nr_matricol, id_pers, id_grupa) VALUES ('12446224', 44, 5);
INSERT INTO studenti (nr_matricol, id_pers, id_grupa) VALUES ('12241166', 45, 5);

select * from studenti;

-- MATERII --
INSERT INTO materii (nume_materie, nr_credite) VALUES ('Analiza Matematica', 2);
INSERT INTO materii (nume_materie, nr_credite) VALUES ('Proiectare Logica', 5);
INSERT INTO materii (nume_materie, nr_credite) VALUES ('Engleza', 2);
INSERT INTO materii (nume_materie, nr_credite) VALUES ('Programarea Calculatoarelor', 6);
INSERT INTO materii (nume_materie, nr_credite) VALUES ('Structuri de date si algoritmi', 6);

select * from materii;

-- NOTE --
INSERT INTO note (id_student, id_cadru_didact, tip_sesiune, tip_examinare, id_materie, nota, situatie) VALUES (1, 1, 'iarna', 'marire', 1, 6, 'admis');

select * from note;

-- MATERII - CADRU DIDACTIC --
INSERT INTO materii_cadru_did (id_materie, id_cadru_didact) VALUES (1, 1);

select * from materii_cadru_did;
