use invatamant_universitar_1;

-- TRIGGERE DML --
CREATE TRIGGER nota_incorecta ON note AFTER UPDATE, INSERT 
AS
BEGIN
	DECLARE @nota FLOAT;
	SELECT @nota = nota 
	FROM note;
	IF (@nota > 10)
	BEGIN
		ROLLBACK TRANSACTION;
		PRINT('Nota nu poate sa fie mai mare decat 10!');
	END;
END;

INSERT INTO note (id_student, id_cadru_didact, tip_sesiune, tip_examinare, id_materie, nota, situatie) VALUES (5, 1, 'iarna', 'marire', 1, 11, 'admis');
select * from note;

CREATE TRIGGER varsta_pensie ON persoane AFTER UPDATE, INSERT 
AS
BEGIN
	DECLARE @varsta FLOAT;
	SELECT @varsta = varsta 
	FROM persoane;
	IF (@varsta > 65)
	BEGIN
		ROLLBACK TRANSACTION;
		PRINT('Personal obligat sa se pensioneze!');
	END;
END;

drop trigger varsta_pensie;

INSERT INTO persoane (cnp, nume, prenume, varsta, adresa) VALUES ('1920257115924', 'Balus', 'Crina', 66, 'Cluj');
select * from persoane;

-- TRIGGERE DDL --
CREATE TRIGGER no_drop_tables
ON DATABASE
FOR DROP_TABLE
AS
	PRINT 'INTERZIS!!! Nu se pot sterge tabele din aceasta baza de date.'
	ROLLBACK;

CREATE TRIGGER no_drop_function
ON DATABASE
FOR DROP_FUNCTION
AS
	PRINT 'INTERZIS!!! A nu se incerca sterge functii din acest sistem!'
	ROLLBACK;
