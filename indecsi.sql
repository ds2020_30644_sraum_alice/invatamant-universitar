use invatamant_universitar_1;


--- INDECSI ---
CREATE INDEX index_for_authentication ON persoane(nume ASC, prenume ASC, cnp);
SELECT nume, prenume, cnp FROM persoane
WITH (INDEX (index_for_authentication));


CREATE UNIQUE INDEX unique_index_for_departments ON departamente(nume_depart ASC);
SELECT nume_depart FROM departamente
WITH (INDEX (unique_index_for_departments));

CREATE NONCLUSTERED INDEX join_index_for_students ON persoane (nume ASC, prenume ASC);
CREATE NONCLUSTERED INDEX join_index_for_students ON studenti (id_student);

SELECT p.nume, p.prenume
FROM persoane p, studenti s
WITH (INDEX (join_index_for_students))
WHERE p.id_pers = s.id_student;
