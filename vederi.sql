use invatamant_universitar_1;

-- VEDERI -- 

-- [1] Toti studenti care au promovat --
CREATE VIEW [Studenti Promovati] AS
SELECT p.nume AS NUME, p.prenume AS PRENUME, n.nota AS NOTA, n.situatie AS SITUATIE
FROM studenti s, note n, persoane p
WHERE s.id_student = n.id_student AND s.id_pers = p.id_pers AND n.nota >= 5;

SELECT * FROM [Studenti Promovati];

-- [2] Toate persoanele peste varsta medie a Universitatii --

CREATE VIEW [Persoane peste media varstei] AS
SELECT nume, prenume, varsta
FROM persoane
WHERE varsta > (SELECT AVG(varsta) FROM persoane);

SELECT * FROM [Persoane peste media varstei] ORDER BY varsta ASC;

-- [3] Materii importante (cu un numar mai mare de credite)--

CREATE VIEW [Materii importante] AS
SELECT nume_materie, nr_credite
FROM materii
WHERE nr_credite BETWEEN 6 AND 8
WITH CHECK OPTION;

SELECT * FROM [Materii importante] ORDER BY nr_credite DESC;
