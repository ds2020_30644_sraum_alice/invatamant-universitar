use invatamant_universitar_1;

--- proceduri stocate ---

-- CREATE --
CREATE PROCEDURE note_admisi_analiza_matematica_sesiune_iarna
	 @nota FLOAT
AS
BEGIN
	INSERT INTO note (id_student, id_cadru_didact, tip_sesiune, tip_examinare, id_materie, nota, situatie) VALUES (2, 1, 'iarna', 'marire', 1, @nota, 'admis');
	INSERT INTO note (id_student, id_cadru_didact, tip_sesiune, tip_examinare, id_materie, nota, situatie) VALUES (3, 1, 'iarna', 'marire', 1, @nota, 'admis');
	INSERT INTO note (id_student, id_cadru_didact, tip_sesiune, tip_examinare, id_materie, nota, situatie) VALUES (4, 1, 'iarna', 'marire', 1, @nota, 'admis');
END;

EXEC note_admisi_analiza_matematica_sesiune_iarna 7;


-- READ --
CREATE PROCEDURE integralist
	@prenume_student VARCHAR(50)
AS
BEGIN
	SELECT n.situatie
	FROM persoane p JOIN studenti s ON p.id_pers = s.id_pers 
	JOIN note n ON s.id_student = n.id_student
	WHERE n.nota >=5 AND p.prenume = @prenume_student
	ORDER BY p.nume ASC;
END;

EXEC integralist 'Alexei';


-- UPDATE --
CREATE PROCEDURE mariri_toamna_analiza_matematica
	@nota_veche FLOAT, @nota_noua FLOAT
AS
BEGIN
	UPDATE note SET nota = @nota_noua WHERE nota = @nota_veche;
END;

EXEC mariri_toamna_analiza_matematica 6, 7;


-- DELETE --
CREATE PROCEDURE studenti_care_au_renuntat_la_studii
	@nume_student VARCHAR(50), @prenume_student VARCHAR(50)
AS
BEGIN 
	DECLARE @id_persoana INTEGER;
	DECLARE @id_note INTEGER;

	SELECT @id_persoana = p.id_pers, @id_note = n.id_note
	FROM persoane p join studenti s on p.id_pers = s.id_pers join note n on s.id_student = n.id_student
	WHERE p.nume = @nume_student AND p.prenume = @prenume_student;

	DELETE FROM persoane WHERE id_pers = @id_persoana;
	DELETE FROM studenti WHERE id_student = @id_persoana;
	DELETE FROM note WHERE id_note = @id_note;
END;

EXEC studenti_care_au_renuntat_la_studii 'Potor', 'Dan';

