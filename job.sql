

--- joburi ---

USE msdb;
GO
EXEC dbo.sp_add_job
 @job_name = N'WeeklyQuizzJob' ;
GO
EXEC sp_add_jobstep
 @job_name = N'WeeklyQuizzJob', 
 @step_name = N'WeeklyQuizzJob_step',
 @subsystem = N'TSQL',
 @command = N'EXEC setQuizzDay',
 @retry_attempts = 5,
 @retry_interval = 5 ;
GO
EXEC dbo.sp_add_schedule
 @schedule_name = N'WeeklyQuizzJob_schedule11',
 @freq_type = 8, --weekly
 @freq_interval = 2, --Monday
 @freq_recurrence_factor = 1,
 @active_start_time = 130000 ; --13:00
GO
EXEC sp_attach_schedule
 @job_name = N'WeeklyQuizzJob',
 @schedule_name = N'WeeklyQuizzJob_schedule11';
GO
EXEC dbo.sp_add_jobserver
 @job_name = N'WeeklyQuizzJob';


CREATE PROCEDURE setQuizzDay
AS
BEGIN
	PRINT 'QUIZZ DAY!';
END;

-- daca dorim sa stergem procedura
drop PROCEDURE setQuizzDay

-- daca dorim sa executam separat aceasta procedura
EXEC setQuizzDay


-- comenzi pentru verificarea job-ului 2 care este facut custom --
use invatamant_universitar_1;
select * from note;



